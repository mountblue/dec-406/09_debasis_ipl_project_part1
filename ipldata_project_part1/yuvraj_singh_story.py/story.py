"""Calculates Yuvraj Sing's runs
Per match of all season

Data(IPL MATCHES DATA) : kaggle.com

Created at : 2019
Author : Debasis
"""

import matplotlib.pyplot as plt
import sys
sys.path.insert(0,"../data")
from main import get_data

deliveries_reader = get_data("../data/deliveries.csv")


def runs_of_batsman(datafile, batsman_name):

    """ Calculates runs scored by a batsman in each match for all season """

    filtered_data = [{"id":match["match_id"], "runs": match["batsman_runs"], "batsman":match["batsman"]} for match in datafile if match["batsman"] == batsman_name]

    ids = list(set([match["id"] for match in filtered_data]))

    mapper = []
    for id  in ids:
        mapper.append(list(filter(lambda x: x["id"] == id, filtered_data)))
    
    runs_per_match =  {}
    for index, array in enumerate(mapper):
        runs_per_match[index] = sum([int(value["runs"]) for value in array])
    return runs_per_match

def create_plot(data):

    labels = list(data.keys())
    sizes = list(data.values())

    fig1, ax1 = plt.subplots()
    ax1.pie(sizes,
            shadow=True, startangle=90)
    ax1.axis('equal') 

    plt.show()

def draw_plot(data_function, plot_function, deliveries_reader, name_of_player):
    number_of_runs_per_match = data_function(deliveries_reader, name_of_player)
    plot_function(number_of_runs_per_match)

draw_plot(runs_of_batsman, create_plot, deliveries_reader, "Yuvraj Singh")
