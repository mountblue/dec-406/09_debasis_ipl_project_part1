"""Calculates the number of wins per team played in
Each season
Data(IPL MATCHES DATA) : kaggle.com

Created at : 2019
Author : Debasis
"""

import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import numpy  as np

from collections import Counter
import sys
sys.path.insert(0,"../data")
from main import get_data

matches_reader = get_data("../data/matches.csv")


def wins_per_team(datafile):

    """ Calculates the num of matches won by each team  """
    
    each_season_data = {}
    set_of_teams = [match["winner"] for match in datafile if match["winner"] != ""]
    
    for match in datafile:
        if not match["season"] in each_season_data:
            each_season_data[match["season"]] = {team:0 for team in set_of_teams}
        else:
            if match["winner"] != "":
                each_season_data[match["season"]][match["winner"]] += 1
    return each_season_data
                                                                                                             
def create_plot(data):
    seasons = sorted(list(data.keys()))
    teams_list = []

    colors = {'Chennai Super Kings':'yellow', 'Deccan Chargers':'black', 'Delhi Daredevils':'red', 'Gujarat Lions':'orange', 'Kings XI Punjab':'brown', 'Kochi Tuskers Kerala':'green', 'Kolkata Knight Riders':'purple', 'Mumbai Indians':'blue', 'Pune Warriors':'grey', 'Rajasthan Royals':'lightblue', 'Rising Pune Supergiant':'pink', 'Rising Pune Supergiants':'salmon', 'Royal Challengers Bangalore':'tomato', 'Sunrisers Hyderabad':'tan'}


    for season in seasons:
        bottom_value = 0
        for team, matches_won in sorted(data[season].items()):
            if team not in teams_list:
                teams_list.append(team)
            plt.bar(season, matches_won, bottom = bottom_value, color=colors[team])
            bottom_value += int(matches_won)
    
    plt.xticks(seasons)
    plt.legend(teams_list, fontsize = 'xx-small')
    plt.tight_layout(rect=[0, 0, 0.75, 1])
    plt.show()
    

  
def draw_plot(data_function, plot_function, file_data):
    winners_data = data_function(file_data)
    create_plot(winners_data)

draw_plot(wins_per_team, create_plot, matches_reader)