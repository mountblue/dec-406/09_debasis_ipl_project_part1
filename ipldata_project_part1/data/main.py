""" Base Module To Parse Any CSV files

Created at : 2019
Author : Debasis
"""

import csv


def get_data(file):

    """ Parses a CSV file and returns a list of each row's data """
    
    datas = []
    with open(file, "r") as reading_file:
        csv_reader = csv.DictReader(reading_file)
        
        for line in csv_reader:
            datas.append(line)
    return datas


