"""Calculates the top ten economical bowlers
of season 2015
Data(IPL MATCHES DATA) : kaggle.com

Created at : 2019
Author : Debasis
"""
import time
import matplotlib.pyplot as plt
import sys
sys.path.insert(0,"../data")
from main import get_data

matches_reader = get_data("../data/matches.csv")
deliveries_reader = get_data("../data/deliveries.csv")


def calculate_economy(bowlers):

    """ Calculates economy of each bowler """

    top_10_economical_bowlers = {}
    for bowler, data  in bowlers.items():
        top_10_economical_bowlers[bowler] = round(data["runs"] /data["overs"], 2)
    return top_10_economical_bowlers
    

    
def get_economical_bowlers(match_data, deliveries_reader, year):

    """ Finds the top economical bowler for a particular year """

    match_ids = set((match["id"] for match in match_data if match["season"] == year))
    bowlers_data = dict()
    bowler_name = None

    for match in deliveries_reader:
        if match["match_id"] in match_ids:
            if not match["bowler"] in bowlers_data:
                runs = int(match["total_runs"]) - (int(match["bye_runs"]) + int(match["legbye_runs"]))
                bowlers_data[match["bowler"]] = {"runs":runs, "overs":0}
                bowler_name = match["bowler"]
            else:
                runs = int(match["total_runs"]) - (int(match["bye_runs"]) + int(match["legbye_runs"]))
                bowlers_data[match["bowler"]]["runs"]  += runs
                if bowler_name != match["bowler"]:
                    bowlers_data[bowler_name]["overs"] += 1
                    bowler_name = match["bowler"]

    
    economy_of_bowlers = calculate_economy(bowlers_data)
    top_economical_bowler = sorted(list(economy_of_bowlers.items()), key=lambda l:l[1])[:10]
    return top_economical_bowler


def create_plot(data):
    plt.bar([value[0] for value  in data], [value[1] for value in data])
    plt.ylabel("num of matches per year")
    plt.xlabel("seasons")
    plt.xticks(rotation=90)
    plt.show()

def draw_plot(data_function, plot_function, match_data, deliveries_reader, year):
    top_economical_bowlers = data_function(match_data, deliveries_reader, year)
    plot_function(top_economical_bowlers)

draw_plot(get_economical_bowlers, create_plot, matches_reader, deliveries_reader, "2015")
