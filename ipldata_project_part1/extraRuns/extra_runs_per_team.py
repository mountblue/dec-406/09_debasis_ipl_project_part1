"""Calculates the extra runs given by each team
In each season
Data(IPL MATCHES DATA) : kaggle.com

Created at : 2019
Author : Debasis
"""


import matplotlib.pyplot as plt
import sys
sys.path.insert(0,"../data")
from main import get_data

matches_reader = get_data("../data/matches.csv")
deliveries_reader = get_data("../data/deliveries.csv")

def get_extra_runs(data_file1, data_file2, year):

    """ Calculates extra runs given by each bowling team for a particular year """

    match_ids = set((match["id"] for match in data_file1 if match["season"] == year ))
    extra_runs = {}

    for match in data_file2:
        if match["match_id"] in match_ids:
            if not match["bowling_team"] in extra_runs:
                extra_runs[match["bowling_team"]] = int(match["extra_runs"])
            else:
                extra_runs[match["bowling_team"]] += int(match["extra_runs"])
    return extra_runs

def create_plot(data):
    plt.bar([key for key  in data.keys()], [value for value in data.values()])
    plt.ylabel("extra runs conceded by each team")
    plt.xticks(rotation=90)
    plt.xlabel("teams")
    plt.show()

def draw_plot(data_function, plot_function, matches_reader, deliveries_reader, year):
    extra_runs_per_team = data_function(matches_reader,deliveries_reader, year)
    plot_function(extra_runs_per_team)

draw_plot(get_extra_runs,create_plot,matches_reader, deliveries_reader, "2016")