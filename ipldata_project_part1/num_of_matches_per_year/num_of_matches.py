"""Calculates the number of matches played
Per season
Data(IPL MATCHES DATA) : kaggle.com

Created at : 2019
Author : Debasis
"""

import matplotlib.pyplot as plt
import sys
sys.path.insert(0,"../data")
from main import get_data

matches_reader = get_data("../data/matches.csv")

def matches_per_season(datafile):

    """ Calculates the number of matches played for each season """

    matches_per_year = {}
    for match in datafile:
        if match["season"]  in matches_per_year:
            matches_per_year[match["season"]] += 1
        else:
            matches_per_year[match["season"]] = 1
    return matches_per_year

def create_plot(data):
    print(data)
    plt.bar(sorted([key for key  in data.keys()]), [value for value in data.values()])
    plt.ylabel("num of matches per year")
    plt.xlabel("seasons")
    plt.show()

def draw_plot(data_function, plot_function, main_data):
    matches_per_year = data_function(main_data)
    plot_function(matches_per_year)

draw_plot(matches_per_season, create_plot, matches_reader)